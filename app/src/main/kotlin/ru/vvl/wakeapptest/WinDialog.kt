package ru.vvl.wakeapptest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.win.*

class WinDialog : DialogFragment() {

    interface Listener {
        fun onWinRepeat()
        fun onWinShare()
    }

    private var listener: Listener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.win, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(STYLE_NO_FRAME, R.style.Dialog)

        listener = parentFragment as Listener

        isCancelable = false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivWinRepeat.setOnClickListener { listener?.onWinRepeat() }
        ivWinShare.setOnClickListener { listener?.onWinShare() }
    }

    override fun onDetach() {
        super.onDetach()

        listener = null
    }
}