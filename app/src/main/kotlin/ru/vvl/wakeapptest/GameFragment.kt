package ru.vvl.wakeapptest

import android.animation.Animator
import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.forEachIndexed
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.game.*
import ru.vvl.wakeapptest.GameFragment.Direction.Companion.updateDirectionAfterLeftClick
import ru.vvl.wakeapptest.GameFragment.Direction.Companion.updateDirectionAfterRightClick
import kotlin.math.max
import kotlin.math.min

class GameFragment : Fragment(R.layout.game), LoseDialog.Listener, WinDialog.Listener {

    private val SPEED = 20f //speed :default 20px/1ms

    private enum class Direction(
        val translationX: Float,
        val translationY: Float,
        val drawable: Int
    ) {
        TOP(0f, -1f, R.drawable.ic_player_top),
        RIGHT(1f, 0f, R.drawable.ic_player_right),
        BOTTOM(0f, 1f, R.drawable.ic_player_bottom),
        LEFT(-1f, 0f, R.drawable.ic_player_left);

        companion object {
            private val values = Direction.values().toList()
            fun Direction.updateDirectionAfterLeftClick() =
                values[if (ordinal == 0) values.lastIndex else ordinal - 1]

            fun Direction.updateDirectionAfterRightClick() =
                values[if (ordinal == values.lastIndex) 0 else ordinal + 1]
        }
    }

    private var player: ImageView? = null
    private var blockSize: Float = 0f
    private val level1 = listOf(0, 1, 5, 6, 7, 8, 9, 10, 11, 12, 13, 18, 22, 23, 27)

    private val completedBlocks = mutableSetOf<Int>()
    private var currentDirection = Direction.TOP
    private var previousBlockIndex = -1

    private val playerHalfSize by lazy { requireView().context.dipI(34f) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        player = vPlayer
        view.post { fillLevel(flLevel) }
        view.postDelayed({ startGame() }, 100)

        vLeftClick.setOnClickListener {
            currentDirection = currentDirection.updateDirectionAfterLeftClick()
        }
        vRightClick.setOnClickListener {
            currentDirection = currentDirection.updateDirectionAfterRightClick()
        }
    }

    private fun fillLevel(levelContainer: FrameLayout) {
        val widthBlockSize = levelContainer.width.toFloat() / 5
        val heightBlockSize = levelContainer.height.toFloat() / 6

        val (defMarginTop, defMarginStart) = if (widthBlockSize < heightBlockSize) {
            //use widthBlockSize
            (levelContainer.height - widthBlockSize.toInt() * 6) / 2 to 0
        } else {
            //use heightBlockSize
            0 to (levelContainer.width - heightBlockSize.toInt() * 5) / 2
        }

        blockSize = min(widthBlockSize, heightBlockSize)

        val blockSizeInt = blockSize.toInt()
        for (i in 0..29) {
            val column = i % 5
            val row = i / 5
            levelContainer.addView(
                ImageView(levelContainer.context).apply {
                    setImageResource(if (i in level1) R.drawable.ic_block else 0)
                },
                FrameLayout.LayoutParams(blockSizeInt, blockSizeInt).apply {
                    topMargin = defMarginTop + row * blockSizeInt
                    marginStart = defMarginStart + column * blockSizeInt
                }
            )
        }
    }

    private fun startGame() {
        if (view == null) return

        currentDirection = Direction.TOP
        completedBlocks.clear()
        previousBlockIndex = -1

        var startPosition: Pair<Float, Float> = 0f to 0f
        flLevel!!.apply {
            for (index in 0 until childCount) {
                val view = getChildAt(index)
                if (index in level1) {
                    view.alpha = 1f
                    val viewRect = Rect()
                    view.getGlobalVisibleRect(viewRect)
                    startPosition =
                        (viewRect.centerX() - playerHalfSize).toFloat() to (viewRect.height() + viewRect.centerY() - playerHalfSize).toFloat()
                }
            }
        }
        player?.apply {
            x = startPosition.first
            y = startPosition.second
        }

        move(true, currentDirection)
    }

    private fun move(justStarted: Boolean, animationDirection: Direction) {
        if (view == null) return

        var currentBlock: View? = null
        var currentBlockIndex = -1
        var currentBlockRect: Rect? = null

        val tempRect = Rect()
        player!!.getGlobalVisibleRect(tempRect)
        val playerCenterX = tempRect.centerX()
        val playerCenterY = tempRect.centerY()

        flLevel.forEachIndexed { index, view ->
            val rect = Rect()
            view.getGlobalVisibleRect(rect)

            if (rect.contains(playerCenterX, playerCenterY) && index in level1) {
                if (currentBlock != null) throw IllegalArgumentException("WTF?")
                currentBlock = view
                currentBlockIndex = index
                currentBlockRect = rect
            }
        }

        if (currentBlockIndex != -1) {
            if (previousBlockIndex != currentBlockIndex &&
                completedBlocks.contains(currentBlockIndex)
            ) {
                showLoseDialog()
                return
            } else {
                completedBlocks.add(currentBlockIndex)
            }
        }

        val lastIndex = completedBlocks.size - 1
        completedBlocks.forEachIndexed { index, i ->
            val view = flLevel.getChildAt(i)
            when (index) {
                lastIndex -> {
                }
                lastIndex - 1 -> {
                    view.alpha = 0.7f
                }
                lastIndex - 2 -> {
                    view.alpha = 0.4f
                }
                else -> {
                    view.alpha = 0f
                }
            }
        }

        if (currentBlock == null && !justStarted) {
            if (completedBlocks.size == level1.size) {
                showWinDialog()
            } else {
                showLoseDialog()
            }
            return
        }

        val newJustStarted = currentBlock == null

        var newDirection = animationDirection
        var newTranslationY = animationDirection.translationY * SPEED
        var newTranslationX = animationDirection.translationX * SPEED

        if (currentBlock != null && animationDirection != currentDirection) {
            val currentBlockCenterX = currentBlockRect!!.centerX()
            val currentBlockCenterY = currentBlockRect!!.centerY()
            when {
                newTranslationX != 0f -> {
                    if ((playerCenterX + newTranslationX > currentBlockCenterX && playerCenterX < currentBlockCenterX) || (playerCenterX + newTranslationX < currentBlockCenterX && playerCenterX > currentBlockCenterX)) {
                        newTranslationX = currentBlockCenterX.toFloat() - playerCenterX
                        newDirection = currentDirection
                    }
                }
                newTranslationY != 0f -> {
                    if (((playerCenterY + newTranslationY) > currentBlockCenterY && playerCenterY < currentBlockCenterY) || ((playerCenterY + newTranslationY) < currentBlockCenterY && playerCenterY > currentBlockCenterY)) {
                        newTranslationY = currentBlockCenterY.toFloat() - playerCenterY
                        newDirection = currentDirection
                    }
                }
            }
        }

        player!!.apply {
            setImageResource(animationDirection.drawable)
            animate()
                .setDuration(1)
                .translationYBy(newTranslationY)
                .translationXBy(newTranslationX)
                .setListener(object : Animator.AnimatorListener {
                    override fun onAnimationCancel(animation: Animator?) {
                    }

                    override fun onAnimationEnd(animation: Animator?) {
                        move(newJustStarted, newDirection)
                    }

                    override fun onAnimationRepeat(animation: Animator?) {
                    }

                    override fun onAnimationStart(animation: Animator?) {
                    }
                })
                .start()
        }
        previousBlockIndex = currentBlockIndex
    }

    private fun showLoseDialog() {
        LoseDialog().show(childFragmentManager, LOSE_DIALOG_TAG)
        childFragmentManager.executePendingTransactions()
    }

    private fun showWinDialog() {
        WinDialog().show(childFragmentManager, WIN_DIALOG_TAG)
        childFragmentManager.executePendingTransactions()
    }

    override fun onLoseRepeat() {
        (childFragmentManager.findFragmentByTag(LOSE_DIALOG_TAG) as LoseDialog).dismissAllowingStateLoss()
        startGame()
    }

    override fun onLoseShare() {
        (childFragmentManager.findFragmentByTag(LOSE_DIALOG_TAG) as LoseDialog).dismissAllowingStateLoss()
        showInDevelopment()
    }

    override fun onWinRepeat() {
        (childFragmentManager.findFragmentByTag(WIN_DIALOG_TAG) as WinDialog).dismissAllowingStateLoss()
        startGame()
    }

    override fun onWinShare() {
        (childFragmentManager.findFragmentByTag(WIN_DIALOG_TAG) as WinDialog).dismissAllowingStateLoss()
        showInDevelopment()
    }

    private fun showInDevelopment() {
        Toast.makeText(requireView().context, "In development", Toast.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        player = null
    }

    fun Context.dipI(value: Float): Int = (value * resources.displayMetrics.density).toInt()

    companion object {
        private const val LOSE_DIALOG_TAG = "lose dialog tag"
        private const val WIN_DIALOG_TAG = "win dialog tag"
    }
}