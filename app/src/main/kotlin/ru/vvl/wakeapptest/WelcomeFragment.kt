package ru.vvl.wakeapptest

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.welcome.*

class WelcomeFragment : Fragment(R.layout.welcome) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        flPlay.setOnClickListener {
            openGame()
        }
    }

    private fun openGame() {
        parentFragmentManager
            .beginTransaction()
            .apply {
                add(R.id.fcvContainer, GameFragment::class.java, null)
                addToBackStack("game")
                commitAllowingStateLoss()
            }
    }
}
